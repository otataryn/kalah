Project uses Java 8 and mavan to build. 

I recommend running it through 
    
    docker-compose up 

If that is impossible for you - use maven:

    mvn clean package && java -jar target/Kalah-1.0-SNAPSHOT.jar

I've implemented the two endpoints required in the assignment. 
You can find API mapping in com.game.api.rest.Application. 

Tests are working and I recommend you take a look at them. 

Project does not require any kind of database. 
Instead I use singleton object KalahEngine with a Map of existing games. 
This is not an oversight - database is an external device, 
and since we do not have requirement of long-term persistence - 
we do not need the database. 

Project core should work regardles of API schema used. 
Therefore Spring is only used inside com.game.api.rest. 
There is a dependency from tests to the api.rest package. 
This is beacuse most of the task requirements are in form of API schema. 