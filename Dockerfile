FROM openjdk:8-jdk-alpine as builder

RUN apk add maven
RUN mkdir /target/
COPY . .
RUN mvn install

FROM openjdk:8-jre-alpine
COPY --from=builder /root/.m2/repository/com/game/Kalah/* /target/
ENTRYPOINT ["java", "-jar", "/target/Kalah-1.0-SNAPSHOT.jar"]
