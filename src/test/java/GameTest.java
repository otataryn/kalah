import com.game.*;
import com.game.api.rest.RestAdapter;
import com.game.kalah.*;
import com.game.kalah.mistakes.CanNotMoveKalah;
import com.game.kalah.mistakes.NoStonesToMove;
import com.game.kalah.mistakes.WrongPlayerMove;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.function.Function;
import java.util.regex.Pattern;

@Test
public class GameTest {
    public void canStartNewGame(){
        GameEngine gameEngine = new KalahEngine();
        GameId gameId = gameEngine.createGame();

        String resourceId = RestAdapter.toRestString(gameId);
        String resourceURI = RestAdapter.toRestURI(gameId);

        String idRegexp = "\\d+";
        Assert.assertTrue(matches(idRegexp, resourceId));
        Assert.assertEquals(RestAdapter.getBaseURI() + "/games/" + resourceId, resourceURI);
        String urlRegexp = "http://[a-zA-Z0-9]+(\\.[a-zA-Z0-9])*(:\\d+)?(/[a-zA-Z0-9]+)*";
        Assert.assertTrue(matches(urlRegexp, RestAdapter.getBaseURI()));
    }

    public void gamesAreDifferent(){
        Function<GameId, String> convertToString = RestAdapter::toRestString;
        Function<GameEngine, String> newGame = convertToString.compose(GameEngine::createGame);

        GameEngine gameEngine = new KalahEngine();
        String resourceId1 = newGame.apply(gameEngine);
        String resourceId2 = newGame.apply(gameEngine);

        Assert.assertNotEquals(resourceId1, resourceId2);
    }

    public void canGetGameState(){
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();

        Game game = gameEngine.getGame(id);

        verifyGame(game, new int[]{6,6,6,6,6,6,0,6,6,6,6,6,6,0});
    }

    public void canMove() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        Move move = new KalahMove(1);
        Game game = gameEngine.makeMove(id, move);


        verifyGame(game, new int[]{0,7,7,7,7,7,1,6,6,6,6,6,6,0});
    }

    public void canChainMoves() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        gameEngine.makeMove(id, new KalahMove(1));
        Game game = gameEngine.makeMove(id, new KalahMove(2));

        verifyGame(game, new int[]{1,0,8,8,8,8,9,6,6,6,6,0,6,0});
    }

    private void verifyGame(Game game, int[] array) {
        GameId id = game.getId();
        String gameJsonString = RestAdapter.toRestString(game);

        JSONObject gameJson = new JSONObject(gameJsonString);
        Assert.assertEquals(gameJson.getString("id"), RestAdapter.toRestString(id));
        Assert.assertEquals(gameJson.getString("url"), RestAdapter.toRestURI(id));
        JSONObject gameStatusJson = gameJson.getJSONObject("status");
        for(int i=0; i<14; i++) {
            Assert.assertEquals(gameStatusJson.getInt(""+(i+1)), array[i]);
        }
    }

    public void canNotMoveOutOfOrder() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        gameEngine.makeMove(id, new KalahMove(1));

        gameEngine.makeMove(id, new KalahMove(2));

        Assert.assertThrows(WrongPlayerMove.class, ()-> gameEngine.makeMove(id, new KalahMove(3)));
    }

    public void canNotMoveOutOfOrderAsSecondPlayer() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        gameEngine.makeMove(id, new KalahMove(1));

        gameEngine.makeMove(id, new KalahMove(2));
        Game game = gameEngine.makeMove(id, new KalahMove(8));
        verifyGame(game, new int[]{1,0,8,8,8,8,9,0,7,7,7,1,7,1});
        game = gameEngine.makeMove(id, new KalahMove(9));
        verifyGame(game, new int[]{1,0,8,8,0,8,9,1,0,8,8,2,8,11});

        Assert.assertThrows(WrongPlayerMove.class, ()-> gameEngine.makeMove(id, new KalahMove(10)));
    }

    public void canNotMoveEmptyHole() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        gameEngine.makeMove(id, new KalahMove(1));
        gameEngine.makeMove(id, new KalahMove(2));
        Assert.assertThrows(NoStonesToMove.class, ()-> gameEngine.makeMove(id, new KalahMove(12)));
    }

    public void canMoveSecondPlayer() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        gameEngine.makeMove(id, new KalahMove(1));
        gameEngine.makeMove(id, new KalahMove(2));
        Game game = gameEngine.makeMove(id, new KalahMove(13));

        verifyGame(game, new int[]{1,0,8,8,8,8,9,7,7,7,7,0,0,2});
    }

    public void canNotMoveKalah() {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        Assert.assertThrows(CanNotMoveKalah.class, ()-> gameEngine.makeMove(id, new KalahMove(7)));
    }

    public void canNotMoveKalahAsSecondPlayer() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();
        gameEngine.makeMove(id, new KalahMove(3));

        Assert.assertThrows(CanNotMoveKalah.class, ()-> gameEngine.makeMove(id, new KalahMove(14)));
    }

    public void canNotMoveOtherKalah() throws GameException {
        GameEngine gameEngine = new KalahEngine();
        GameId id = gameEngine.createGame();

        Assert.assertThrows(WrongPlayerMove.class, ()-> gameEngine.makeMove(id, new KalahMove(14)));

        gameEngine.makeMove(id, new KalahMove(3));

        Assert.assertThrows(WrongPlayerMove.class, ()-> gameEngine.makeMove(id, new KalahMove(7)));
    }


    private boolean matches(String urlRegexp, String baseURI) {
        return Pattern.compile(urlRegexp).matcher(baseURI).matches();
    }
}
