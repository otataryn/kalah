package com.game;

import java.util.Objects;

public class GameId {
    private String id;
    public GameId(int gameId) {
        id = Integer.toString(gameId);
    }

    @Override
    public String toString() {
        return "com.kalah.GameId{" +
                "id='" + id + '\'' +
                '}';
    }

    public String print() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameId gameId = (GameId) o;
        return Objects.equals(id, gameId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
