package com.game.api.rest;

import com.game.Game;
import com.game.GameId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class RestAdapter {
    public static String toRestString(GameId gameId) {
        return gameId.print();
    }

    public static String toRestURI(GameId gameId) {

        return getBaseURI()+"/games/"+ gameId.print();
    }

    @Autowired
    static Environment environment;

    public static String getBaseURI() {
        if(environment ==null){
            return "http://localhost:8080";
        }
        return "http://"+environment.getProperty("local.server.host")+":"+environment.getProperty("local.server.port");
    }

    public static String toRestString(Game game) {
        return String.format("{\"id\":\"%s\",\"url\":\"%s\",\"status\":{%s}}",
                toRestString(game.getId()),
                toRestURI(game.getId()),
                game.print());
    }

    public static String getIdResponseFormat() {
        return  "{\"id\":\"%s\", \"uri\":\"%s\"}";
    }
}
