package com.game.api.rest;

import com.game.*;
import com.game.kalah.KalahEngine;
import com.game.kalah.KalahMove;
import com.game.NotFound;
import com.game.kalah.mistakes.CanNotMoveKalah;
import com.game.kalah.mistakes.MoveOutOfBoard;
import com.game.kalah.mistakes.NoStonesToMove;
import com.game.kalah.mistakes.WrongPlayerMove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@SpringBootApplication
@RestController()
@RequestMapping("/")
public class Application implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args){
        System.out.print("          _.----.                          \n" +
                "       .-\"       \\-.                       \n" +
                "      /           ; \\                      \n" +
                "     :           /:  \\                     \n" +
                "     ;         .'  ;  ;                    \n" +
                "     ;      .-\"    :  :                    \n" +
                "    :   _.+(   .-- :  :                    \n" +
                "    ;  ;   ' :  :                    \n" +
                "    ;  :           ;  ;                    \n" +
                "    :   ;    -    :  :                     \n" +
                "     )  '   .-.   '  :                     \n" +
                "    (    '. `\"' .'   ;                     \n" +
                "     \"-._.:`---':-\"-.'+'                   \n" +
                "          ;     ;    \"                     \n" +
                "   _..__.-. -. (:                          \n" +
                " ,'   .:(o);     \"-._                      \n" +
                " :    _: 0 ;        \\`.                    \n" +
                " ;  .'/.\\-/-.        `:                    \n" +
                ":  : :  -U--:\"-.  \\    ;                   \n" +
                ";  ; :  ----;   \"-.L.-\" \\                  \n" +
                "'. '  \\ ---(      ;O:    ;                 \n" +
                "  \\ '. '-;-'      :-:    :                 \n" +
                "   `. \"\"/         ; :    ;                 \n" +
                "     \"\"T      .-\":  :`. /                  \n" +
                "       :  --\"\"   :   ; Y                   \n" +
                "        ;        ;   : :                   \n" +
                "        :       :     ; ;                  \n" +
                "         ;      :   ; : :                  \n" +
                "         :      ;   :  ; \\                 \n" +
                "          ;    :    ;  :  \\_               \n" +
                "          :    :        \\  \\\"-.            \n" +
                "          ;    ;         \\  `. \"-.         \n" +
                "         :    :     c     \\   `./\"-._      \n" +
                "         ;    :            \\    \\    \"-.   \n" +
                "        :     ;             `.   ;-.  -.`. \n" +
                "        :    :       __..--\"\" \\  :  `.\\.`.\\\n" +
                "        ;    :_..--\"\";  ;  _.-'\\  ;   \")))T\n" +
                "       :     ;      _L.-'\"\"     ; :    '-='\n" +
                "       ;    :_..--\"\"            :  ;       \n" +
                "      /     ;                   ;; :       \n" +
                "    .'     /                    ;: J       \n" +
                "    `.    /                     ;'\"        \n" +
                "      :-.'         /\\           ;          \n" +
                "      ;           /  ;          :          \n" +
                "     :           /   :          :          \n" +
                "     ;          /     ;         :          \n" +
                "    :          /      ;         :          \n" +
                "    ;         /       :         :          \n" +
                "   :         /        :         :         \n");
    }

    @PutMapping("/games/{gameId}/pits/{pitId}")
    public ResponseEntity<String> makeAMove(@PathVariable String gameId, @PathVariable String pitId) throws Exception {
        KalahMove move;
        GameId game;
        try{
            move = new KalahMove(Integer.parseInt(pitId));
        }catch (Exception e){
            throw new Exception("Cannot convert "+ pitId+" to a pit number. com.kalah.Move is invalid");
        }

        try{
            game = new GameId(Integer.parseInt(gameId));
        }catch (Exception e){
            throw new Exception("Cannot convert "+ gameId +" to a game number. com.kalah.Move is invalid");
        }

        Game result = gameEngine.makeMove(game, move);
        return new ResponseEntity<>(RestAdapter.toRestString(result), HttpStatus.OK);

    }

    @Autowired
    GameEngine gameEngine;

    @PostMapping("/games")
    public ResponseEntity<String> createNewGame() {
        GameId id = gameEngine.createGame();
        String response = String.format(
                RestAdapter.getIdResponseFormat(),
                RestAdapter.toRestString(id),
                RestAdapter.toRestURI(id));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ExceptionHandler(value = NotFound.class)
    public ResponseEntity<Object> exception(NotFound exception) {
        return new ResponseEntity<>("Game Not Found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = WrongPlayerMove.class)
    public ResponseEntity<Object> exception(WrongPlayerMove exception) {
        return new ResponseEntity<>("Wrong Player", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NoStonesToMove.class)
    public ResponseEntity<Object> exception(NoStonesToMove exception) {
        return new ResponseEntity<>("No stones to move at this location", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = MoveOutOfBoard.class)
    public ResponseEntity<Object> exception(MoveOutOfBoard exception) {
        return new ResponseEntity<>("Move should be inside the board. i.e. 1-14", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CanNotMoveKalah.class)
    public ResponseEntity<Object> exception(CanNotMoveKalah exception) {
        return new ResponseEntity<>("You can't move Kalah. Try one of the pits instead. ", HttpStatus.BAD_REQUEST);
    }


}

@Configuration
class AppConfig {

    @Bean
    public GameEngine gameEngine(){
        return new KalahEngine();
    }
}
