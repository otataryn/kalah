package com.game;

public interface Game {
    GameId getId();

    String print();
}
