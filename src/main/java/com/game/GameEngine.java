package com.game;

import com.game.kalah.mistakes.InvalidMove;

public interface GameEngine {
    GameId createGame();

    Game getGame(GameId id);

    Game makeMove(GameId id, Move move) throws NotMyMove, InvalidMove, NotFound;
}
