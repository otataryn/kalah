package com.game.kalah;

import com.game.*;
import com.game.kalah.mistakes.InvalidMove;

import java.util.HashMap;
import java.util.Map;

public class KalahEngine implements GameEngine {
    private int gameCounter = 0;
    private final Map<GameId, KalahGame> gamesMap = new HashMap<>();

    synchronized public GameId createGame() {
        gameCounter ++;
        GameId id = new GameId(gameCounter);
        KalahGame game = new KalahGame(id);
        gamesMap.put(id, game);
        return id;
    }

    @Override
    public KalahGame getGame(GameId id) {
        return gamesMap.get(id);
    }

    @Override
    public Game makeMove(GameId id, Move move) throws NotMyMove, InvalidMove, NotFound {
        if(! (move instanceof KalahMove)){
            throw new NotKalahMove();
        }

        KalahGame game = getGame(id);
        if(game == null){
            throw new NotFound();
        }
        synchronized (game) {
            game = game.applyMove((KalahMove) move);
            return game;
        }

    }
}
