package com.game.kalah;

import com.game.Game;
import com.game.GameId;
import com.game.kalah.mistakes.CanNotMoveKalah;
import com.game.kalah.mistakes.MoveOutOfBoard;
import com.game.kalah.mistakes.NoStonesToMove;
import com.game.kalah.mistakes.WrongPlayerMove;

import java.util.stream.IntStream;

public class KalahGame implements Game {
    private final GameId id;
    private final int stoneCount = 6;
    private boolean firstPlayerTurn = true;
    private int kalahSize = 7;

    private final int[] holes = new int[kalahSize*2];


    public KalahGame(GameId id) {
        this.id = id;
        for(int i = 0; i<kalahSize*2; i++){
            if((i+1)%kalahSize != 0){
                holes[i] = stoneCount;
            }else{
                holes[i] = 0;
            }
        }
    }

    @Override
    public GameId getId() {
        return id;
    }

    @Override
    public String print() {
        int i = 0;
        return IntStream.iterate(0, a-> a+1).limit(kalahSize*2).mapToObj(a-> "\""+(a+1)+"\":\""+holes[a]+"\"").reduce((a,b)->a+","+b).orElse("");
    }

    public KalahGame applyMove(KalahMove move) throws NoStonesToMove, WrongPlayerMove, CanNotMoveKalah, MoveOutOfBoard {
        if((move.getHoleId() <1) || move.getHoleId() >kalahSize*2){
            throw new MoveOutOfBoard();
        }

        if((move.getHoleId() <=kalahSize) != firstPlayerTurn){
            throw new WrongPlayerMove();
        }
        if((move.getHoleId() ==kalahSize)||move.getHoleId()==kalahSize*2){
            throw new CanNotMoveKalah();
        }
        int holeId = move.getHoleId()-1;
        int stonesToRelocate = holes[holeId];
        if(stonesToRelocate == 0){
            throw new NoStonesToMove();
        }

        holes[holeId] = 0;
        int lastHoleId = moveStonesAround(holeId, stonesToRelocate);
        if(isEndOfTurn(lastHoleId)){
            if(isCapture(lastHoleId)){
                capture(correspondingEnemyHole(lastHoleId));
                capture(ownHoleId(lastHoleId));
            }
            switchTurn();
        }

        return this;
    }

    private void switchTurn() {
        firstPlayerTurn = !firstPlayerTurn;
    }

    private void capture(int hole) {
        holes[ownKalahId()]+=holes[hole];
        holes[hole] = 0;
    }

    private int correspondingEnemyHole(int lastHoleId) {
        return (kalahSize - 1) - (lastHoleId +1)%kalahSize + enemyOffset();
    }

    private int enemyOffset() {
        return !firstPlayerTurn?0:kalahSize;
    }

    private int ownKalahId() {
        return (kalahSize - 1) + (playerOffset());
    }

    private int ownHoleId(int lastHoleId) {
        return lastHoleId %kalahSize + playerOffset();
    }

    private int playerOffset() {
        return firstPlayerTurn?0:kalahSize;
    }

    private boolean isCapture(int lastHoleId) {
        return holes[ownHoleId(lastHoleId)] == 1;
    }

    private boolean isEndOfTurn(int lastHoleId) {
        return (lastHoleId+1)%kalahSize != 0;
    }

    private int moveStonesAround(int holeId, int stonesToRelocate) {
        while(stonesToRelocate-->0){
            holes[(++holeId)%kalahSize + playerOffset()]++;
        }
        return holeId;
    }
}
