package com.game.kalah;

import com.game.Move;

public class KalahMove implements Move {
    private final int holeNumber;

    public KalahMove(int holeNumber) {
        this.holeNumber= holeNumber;
    }

    public int getHoleId() {
        return holeNumber;
    }
}
